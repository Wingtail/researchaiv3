import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from scipy import sparse
A =  np.array([[0, 1, 0, 0, 1], [0, 0, 1, 1, 1],[1, 1, 0, 1, 0]])
A_sparse = sparse.csr_matrix(A)

B = np.array([[1,0,0,1,0]])

similarities = cosine_similarity(B, A_sparse, dense_output=False)
print('pairwise dense output:\n {}\n'.format(similarities))

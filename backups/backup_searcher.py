import requests
from bs4 import BeautifulSoup
import threading
import re
from nltk.util import ngrams
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer as tfidf
from scipy import spatial
import numpy as np
from nltk.tokenize import RegexpTokenizer
import spacy
import nltk
import torch
from pytorch_pretrained_bert import BertTokenizer, BertForQuestionAnswering, BertModel
import logging
import pickle

from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from abc import ABC, abstractmethod


class NLP(ABC):
    def __init__(self):
        super(NLP, self).__init__()
        self.models = []
        self.model = None
        print('loading QA model')
        #with open('BertQA.p', 'rb') as pfile:
        #    model = pickle.load(pfile)
            #self.models.append(model)
        #    self.model = model
        #    self.query = None
        #    self.paragraph = None
        print('loading complete')
    def cosSimilarity(self, elem):
        return 1-spatial.distance.cosine(elem[1],self.query)

    def getBertQAModelCopy(self, n): #going to do threading
        with open('BertQA.p', 'rb') as pfile:
            print('creating model ', n)
            model = pickle.load(pfile)
            print('complete')
            return model

class Search(NLP):
    def __init__(self, extend):
        NLP.__init__(self)
        self.question = None
        self.regToken = RegexpTokenizer(r'\w+')
        logging.basicConfig(level=logging.INFO)
        self.article = {} #(url, text)
        self.query = None
        self.links = [] #consider removing this
        self.urlExtend = extend
        self.nWorkers = 3
        self.nlp = spacy.load("en", disable=['parser', 'ner'])
        # Load pre-trained modelself.tokenizer (vocabulary)
        self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
        self.tf = tfidf(stop_words = None, ngram_range=(2,2))
        #with ThreadPoolExecutor(max_workers=10) as executor:
        #    res = executor.map(self.getBertQAModelCopy, [i for i in range(0,self.nWorkers)])
        #    self.models = list(res)
        for i in range(0,self.nWorkers):
            self.models.append(self.getBertQAModelCopy(i))
    def getval(element):
        return element[1]

    def ask(self, question=None):
        if(question == None):
            question = self.question
        self.article = {}
        self.links = []
        self.question = question
        self.getArticles(self.getKws())
        paragraphs = self.retrieveBestParagraphs(5)
        answers, tokenQuestion = self.getAnswerCandidates(paragraphs)
        statement = self.getFinalAnswer(answers, tokenQuestion)
        return statement

    @abstractmethod
    def getLinks(self, searchKw, limit=20):
        pass

    def getKws(self):
        words = self.question.lower()
        words = " ".join([token.lemma_ for token in self.nlp(words)])
        words = self.regToken.tokenize(words)
        stopWords = stopwords.words('english')
        searchKws = [word for word in words if word not in stopWords]
        return searchKws

    def getText(self, url):
        resp = requests.get(url)
        htmlTxt = resp.text
        soup = BeautifulSoup(htmlTxt, 'lxml')
        content = []

        for paragraph in soup.find_all('p'):
            content.append(paragraph.text)
        #self.article[url] = ' '.join(content)
        print(url," complete")
        return content

    def getArticles(self, searchKws): #appends articles to links

        with ThreadPoolExecutor(max_workers=10) as executor:
            res = executor.map(self.getLinks, searchKws)
            links = list(res)
        for linkBatch in links:
            self.links.extend(linkBatch)

        print('self links: ', self.links)
        urls = [self.urlExtend+extend for extend in self.links]
        with ThreadPoolExecutor(max_workers=10) as executor:
            res = executor.map(self.getText, urls)

        texts = list(res)

        for i in range(0,len(texts)):
            self.article[urls[i]] = ' '.join(texts[i])
        return

    def retrieveBestParagraphs(self, k):
        tf = tfidf(stop_words = None, ngram_range=(2,2))
        for key in self.article.keys():
            s = self.article[key]
            s = s.lower()

            s = re.sub(r'[^a-zA-Z0-9.?!,()[]-:]', ' ', s)
            self.article[key] = s
            #print(s)
            #tokens = [token for token in s.split(" ") if token != ""]
            #tokens = [word for word in tokens if word not in stopWords] #consider using stopwords or not

            tf = tfidf(stop_words = None, ngram_range=(2,2))
            tfs = tf.fit_transform(self.article.values())

            count = 0
            keys = self.article.keys()

            documentRepresentation = []

            matrix = np.asarray(tfs.todense())
            #print(matrix[1].size)
            for key in self.article.keys():
                documentRepresentation.append((key, matrix[count]))
                count += 1

        response = tf.transform([self.question])
        self.query = np.asarray(response.todense())

        documentRepresentation.sort(key=self.cosSimilarity, reverse=True) #sort document
        for document in documentRepresentation:
            print(document[0],"  ",1-spatial.distance.cosine(document[1], self.query))

        return documentRepresentation[0:k]

    def vectorizeDocument(self, documentRepresentation):
        documents = [] #(link, tokens, vectorList)
        for doc in documentRepresentation:
            text = self.article[doc[0]]
            tokens = ['[CLS]'].extend(self.tokenizer.tokenize(text))
            tokens.append('[SEP]')
            segmentId = [0 for i in range(0,len(tokens))]
            periodIndices = [i for i in range(0,len(tokens)) if tokens[i] in ['.', '?', '!', ';']]
            
            splice = []
            if(len(tokens) > 512):
                n = 0
                val = 0
                for i in periodIndices:
                    if(i-(512*n) <= 512):
                        val = i
                    else:
                        splice.append(val)
                        n += 1
                        val = i
            tokenIds = []
            for i in range(0,len(splice)-1):
                tokenId = self.tokenizer.convert_tokens_to_ids(tokens[splice[i]+1:splice[i+1]])
                tokenIds.append(tokenId)
            #start segment
            tokenIds.append(self.tokenizer.convert_tokens_to_ids(tokens[0,splice[0]]))
            #end segment
            if(splice[len(splice)-1] < len(tokens)-1):
                tokenIds.append(self.tokenizer.convert_tokens_to_ids(tokens[tokens[splice[len(splice)-1]]:len(tokens)]))
        
            for tokenId in tokenIds:
                tokens_tensor = torch.tensor([tokenId])
                segments_tensors = torch.tensor([[0 for i in range(0,len(tokenId))]])
                with torch.no_grad():
                    encoded_layers, _ = self.bertRaw(tokens_tensor, segments_tensors)
        return

    def tokenizeParagraph(self, tokenQuestion, paragraph):
        paragraph = self.tokenizer.tokenize(paragraph)
        tokens = []
        tokens.extend(['[CLS]'])
        tokens.extend(tokenQuestion)
        tokens.extend(['[SEP]'])
        tokens.extend(paragraph)
        tokens.extend(['[SEP]'])

        #print(tokens)

        i=0 #Clean the fucking code
        segments = []
        while (tokens[i] != '[SEP]'):
            segments.append(0)
            i+=1
        segments.append(0)
        i+=1
        count = 0
        while (tokens[i] != '[SEP]'):
            segments.append(1)
            count+=1
            i+=1
        segments.append(1)
        i+=1

        tokenId = self.tokenizer.convert_tokens_to_ids(tokens)
        tokensTensor = torch.tensor([tokenId])
        segmentTensor = torch.tensor([segments])
        return (tokensTensor, segmentTensor, tokenId)

    def answer(self, tup, model=None, getSimple=False): #parameters in token form
        #question = 'Who ruled the duchy of Normandy'

        #sentence = "[CLS] "+question+" [SEP] "+paragraph+" [SEP]"
        #tokens =self.tokenizer.tokenize(sentence)

        tokensTensor = tup[0]
        segmentTensor = tup[1]
        print('starting')
        if(model==None):
            with torch.no_grad():
                prediction = self.model(tokensTensor, segmentTensor)
        else:
            with torch.no_grad():
                prediction = model(tokensTensor, segmentTensor)
        
        print('complete')
        return prediction
    def getAnswerCandidates(self, documentRepresentation):
        n=None
        room = 200

        answers = []
        batches = []
        tokenQuestion =self.tokenizer.tokenize(self.question)
        qSize = len(tokenQuestion)
        for i in range(0,len(documentRepresentation)):
            #check if paragraph is too long
            text = self.article[documentRepresentation[i][0]]
            text = text.rstrip()
            words = self.tokenizer.tokenize(text) #tokenizing article


            if(len(words) >= 512-qSize-3):
                maxLen = 512-qSize-3
                bound = 0
                while(bound < len(words)-maxLen):
                    upper = bound+maxLen
                    if(words[upper].find("##")!=-1):
                        upper-=1
                    segment = words[bound:upper]
                    #print(segment)
                    segment = self.tokensToText(segment)
                    batches.append(segment)
                    bound = upper

                segment = words[bound:len(words)]
                segment = self.tokensToText(segment)
                batches.append(segment) #group strings to perform tfidf
            else:
                batches.append(text)

        tfs = self.tf.fit_transform(batches)

        matrix = np.asarray(tfs.todense())
        batch = [vector for vector in matrix]

        b = [(batches[i],batch[i]) for i in range(0,len(batch))]

        response = self.tf.transform([self.question])
        self.query = np.asarray(response.todense())

        #print(query.size)

        b.sort(key=self.cosSimilarity, reverse=True) #sort document

        b = [b[i] for i in range(0, min(10,len(b)))] #need to set to variable 5
        b = [paragraph[0].rstrip() for paragraph in b]
        #print(len(b))

        #executing multiprocessing
        batches = b
        answers = []
        for batch in batches:
            tokens = self.tokenizeParagraph(tokenQuestion, batch)
            answers.append(tokens) 

        processors = [self.models[i%len(self.models)] for i in range(0,len(answers))]
        #print(processors)
        predictions = []

        with ThreadPoolExecutor(max_workers=len(self.models)) as executor:
            res = executor.map(self.answer, answers, processors)


        result = list(res)
        predictions = result
        getSimple = False;
        candidates = []
        count = 0
        for prediction in predictions:
            tokenId = [i.item() for i in answers[count][0][0]]
            answer = self.segToSentence(tokenId, prediction, tokenQuestion)
            candidates.append(answer)
            count +=1
        print('printing answers:')
        print(candidates)
        print('answers printed')


        return (candidates, tokenQuestion)

    def segToSentence(self, tokenId, prediction, tokenQuestion, getSimple=False):
            score = min(torch.max(prediction[0]).item(),torch.max(prediction[1]).item())
            tokenId = self.tokenizer.convert_ids_to_tokens(tokenId) 
            #print(tokenId)
            stopPuncs = ['.','?','!','[SEP]','[CLS]']
            startIndex = torch.argmax(prediction[0]).item()
            endIndex = torch.argmax(prediction[1]).item()
            #print(startIndex,' ', endIndex)
            answer = []
            if(getSimple==False):
                if(startIndex <= len(tokenQuestion) or endIndex <= len(tokenQuestion)):
                    answer = []
                else:
                    while(endIndex < len(tokenId) and tokenId[endIndex] not in stopPuncs):
                        endIndex+=1
                    while(startIndex > 0 and tokenId[startIndex] not in stopPuncs):
                        startIndex-=1
                    answer = [tokenId[i] for i in range(startIndex+1, endIndex)]
            else:
                answer = [tokenId[i] for i in range(startIndex, endIndex)]
            return answer

    def tokensToText(self, tokens):
        isCitation = False
        processString = []
        for string in tokens:
            if(string ==']'):
                isCitation = False
            elif(string=='['):
                isCitation = True
            elif(isCitation==False):
                if(string.find('##')!=-1):
                    string = string.replace('##',"")
                    if(len(processString)>0):
                        processString[len(processString)-1] = processString[len(processString)-1]+string
                elif(string in [',','.']):
                    if(len(processString)>0):
                        processString[len(processString)-1] = processString[len(processString)-1]+string
                else:
                    processString.append(string)

        statement = ' '.join(processString)
        return statement

    def getFinalAnswer(self, answers, tokenQuestion): #returns in full text format
        answer = [phrase for phrase in answers if (len(phrase)>0)]
        ans = []
        for a in answer:
            ans.extend(a)
            ans.append('.')
        print('final answer: ',ans)
        paragraph = self.tokensToText(ans)

        tokens = self.tokenizeParagraph(tokenQuestion, paragraph)

        
        prediction = self.answer(tokens, self.models[0])

        tokenId = [i.item() for i in tokens[0][0]]
        answer = self.segToSentence(tokenId, prediction, tokenQuestion)
        statement = self.tokensToText(answer)


        print(statement)
        return statement






class WikiSearch(Search):
    def __init__(self):
        print('initializing wikisearch')
        Search.__init__(self, "https://en.wikipedia.org")

    def getLinks(self, searchKw, limit=20):
        #threadLock.acquire()
        url = "https://en.wikipedia.org/w/index.php?search="+searchKw+"&title=Special%3ASearch&profile=advanced&fulltext=1&advancedSearch-current={}&ns0=1"

        resp = requests.get(url)
        htmlTxt = resp.text
        #print(limit)
        soup = BeautifulSoup(htmlTxt, 'lxml')
        #retrieve links of search query
        count = 0
        links = []
        for a in soup.find_all('div'):
            #print(a.attrs.keys())
            if('class' in a.attrs.keys()):
                if('mw-search-result-heading' in a.attrs['class']):
                    b = a.find('a')
                    if(count < limit):
                        links.append(b.attrs['href'])
                        count += 1
                    else:
                        #print(count)
                        #threadLock.release()
                        return links
        return links


#ass = Assistant()
#ass.run()
#search = WikiSearch()
#search.ask("who is louis xvi")

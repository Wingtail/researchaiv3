
from gtts import gTTS
import speech_recognition as sr
import os
import webbrowser
#import smtplib
from pygame import mixer
import pygame as pg
import zerorpc

class Assistant:
    def __init__(self):
        self.mic = sr.Microphone()
        self.r = sr.Recognizer()
        self.wiki = WikiSearch()
        with self.mic as source:
            self.r.adjust_for_ambient_noise(self.mic, duration=1)

    def talk(self, audio):
        print(audio)
        mixer.init(27000)
        speech = gTTS(text=audio, lang='en')
        speech.save('audio.mp3')
        clock = pg.time.Clock()
        try:
            mixer.music.load('audio.mp3')
        except pg.error:
            print('Speech error!')
        mixer.music.play()
        while mixer.music.get_busy():
            clock.tick(30)

    def speech2T(self):
        with self.mic as source:
            print('Ready listening!') 
            self.r.adjust_for_ambient_noise(self.mic, duration=0.5)
            audio = self.r.listen(source)
        try:
            query = self.r.recognize_google(audio)
            print(query)

        except self.mic.UnknownValueError:
            command = self.speech2T()
        query = query.split(" ")
        query = [word for word in query if word not in ["um","uh","uhh","umm"]]
        query = ' '.join(query)
        return query

    def activate(self, phrase):
        with self.mic as source:
            try:
                self.r.adjust_for_ambient_noise(self.mic, duration=0.5)
                audio = self.r.listen(source, timeout=4)
                text = self.r.recognize_google(audio)
                if (text.lower() == phrase):
                    return True
                else:
                    return False
            except sr.UnknownValueError:
                self.r.adjust_for_ambient_noise(source)
                print('adjusting sound')

    def run(self):
        #Run BERT or any other classifier that comprehends speech
        #cross reference which command does the query best match
        self.talk('Activated')

        c = zerorpc.Client()
        c.connect("tcp://35.230.65.161:55556")
        while True:
            #if(self.friend == None):
            #    self.talk("Hi, you are new")
            #    self.talk("I can listen when the message Ready to listen pops up")
            #    self.talk("Lets see if you understand: What's your name")
            #    self.friend = self.speech2T()
            #    speech = "Hi, "+self.friend+" I am Friday"
            #    self.talk(speech)
            #    self.talk("Call 'friday' and I will listen to your question and search it")
            #    self.talk("Ask me anything, and I will deep search wikipedia to grab answers for you")
            #    self.talk("to exit, say bye")
            #    self.talk("you should know that I take a while searching answers")
                #self.talk("When I am done searching, I will remind you about your question and answer")
            if self.activate('friday') == True:
                try:
                    self.talk('Yes?')
                    query = self.speech2T()
                    if(query == "bye"):
                        sp = "bye "+self.friend
                        self.talk(sp)
                        return
                    self.talk('deep searching wikipedia...')
                    self.talk('I will notify you when I am done searching')
                    answer = c.search(query)
                    if(answer != None):
                        self.talk(self.wiki.ask(query))
                    else:
                        self.talk("Sorry, I can't find an answer.")

                except sr.UnknownValueError:
                    self.talk('Please repeat that for me. I can not hear you')
                    command = speech2T()

ass = Assistant()
ass.run()
